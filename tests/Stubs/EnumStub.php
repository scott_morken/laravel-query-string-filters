<?php

namespace Tests\Smorken\QueryStringFilter\Stubs;

enum EnumStub: string
{
    case TEST = 'test';
}
