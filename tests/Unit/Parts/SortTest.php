<?php

namespace Tests\Smorken\QueryStringFilter\Unit\Parts;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Smorken\QueryStringFilter\Constants\SortDirection;
use Smorken\QueryStringFilter\Parts\Sort;

class SortTest extends TestCase
{
    public function test_array_sort_with_direction_set(): void
    {
        $request = (new Request)->merge(['sort' => ['-name']]);
        $sut = new Sort('name');
        $sut->setRequest($request);
        $sortVO = $sut->get();
        $this->assertEquals('name', $sortVO->value);
        $this->assertEquals(SortDirection::DESCENDING, $sortVO->direction);
        $this->assertEquals('-name', (string) $sut);
    }

    public function test_array_sort_without_direction_set(): void
    {
        $request = (new Request)->merge(['sort' => ['name']]);
        $sut = new Sort('name');
        $sut->setRequest($request);
        $sortVO = $sut->get();
        $this->assertEquals('name', $sortVO->value);
        $this->assertEquals(SortDirection::ASCENDING, $sortVO->direction);
        $this->assertEquals('+name', (string) $sut);
    }

    public function test_array_sort_without_direction_set_from_array(): void
    {
        $request = ['sort' => ['name']];
        $sut = new Sort('name');
        $sut->setRequest($request);
        $sortVO = $sut->get();
        $this->assertEquals('name', $sortVO->value);
        $this->assertEquals(SortDirection::ASCENDING, $sortVO->direction);
        $this->assertEquals('+name', (string) $sut);
    }

    public function test_no_sort_and_no_default(): void
    {
        $request = new Request;
        $sut = new Sort('name');
        $sut->setRequest($request);
        $this->assertNull($sut->get());
    }

    public function test_string_sort_with_direction_set(): void
    {
        $request = (new Request)->merge(['sort' => '-name']);
        $sut = new Sort('name');
        $sut->setRequest($request);
        $sortVO = $sut->get();
        $this->assertEquals('name', $sortVO->value);
        $this->assertEquals(SortDirection::DESCENDING, $sortVO->direction);
        $this->assertEquals('-name', (string) $sut);
    }

    public function test_string_sort_without_direction_set(): void
    {
        $request = (new Request)->merge(['sort' => 'name']);
        $sut = new Sort('name');
        $sut->setRequest($request);
        $sortVO = $sut->get();
        $this->assertEquals('name', $sortVO->value);
        $this->assertEquals(SortDirection::ASCENDING, $sortVO->direction);
        $this->assertEquals('+name', (string) $sut);
    }
}
