<?php

namespace Tests\Smorken\QueryStringFilter\Unit\Parts;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Smorken\QueryStringFilter\Parts\With;

class WithTest extends TestCase
{
    public function test_not_set(): void
    {
        $request = new Request;
        $sut = new With('foo');
        $sut->setRequest($request);
        $this->assertEquals(null, $sut->get());
    }

    public function test_include_with_string_value(): void
    {
        $request = (new Request)->merge(['include' => 'foo']);
        $sut = new With('foo');
        $sut->setRequest($request);
        $this->assertEquals('foo', $sut->get());
    }

    public function test_include_with_string_value_from_array(): void
    {
        $request = ['include' => 'foo'];
        $sut = new With('foo');
        $sut->setRequest($request);
        $this->assertEquals('foo', $sut->get());
    }

    public function test_include_with_array_value(): void
    {
        $request = (new Request)->merge(['include' => ['foo']]);
        $sut = new With('foo');
        $sut->setRequest($request);
        $this->assertEquals('foo', $sut->get());
    }
}
