<?php

namespace Tests\Smorken\QueryStringFilter\Unit\Parts;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Smorken\QueryStringFilter\Concerns\Conversion;
use Smorken\QueryStringFilter\Parts\Filter;
use Tests\Smorken\QueryStringFilter\Stubs\EnumStub;

class FilterTest extends TestCase
{
    public function test_filter_enum_value_with_default_default(): void
    {
        $request = new Request;
        $sut = new Filter('foo', null, EnumStub::TEST);
        $sut->setRequest($request);
        $this->assertEquals('test', (string) $sut);
    }

    public function test_filter_string_value_with_default_default(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = new Filter('foo');
        $sut->setRequest($request);
        $this->assertEquals('bar', (string) $sut);
    }

    public function test_filter_with_array(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => ['bar', 'biz']]]);
        $sut = new Filter('foo');
        $sut->setRequest($request);
        $this->assertEquals('bar,biz', (string) $sut);
    }

    public function test_filter_with_array_conversion(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = new Filter('foo', null, null, Conversion::ARRAY);
        $sut->setRequest($request);
        $this->assertEquals(['bar'], $sut->get());
    }

    public function test_filter_with_boolean_conversion(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => '1']]);
        $sut = new Filter('foo', null, null, Conversion::BOOLEAN);
        $sut->setRequest($request);
        $this->assertEquals(true, $sut->get());
    }

    public function test_filter_with_default(): void
    {
        $request = new Request;
        $sut = new Filter('foo', null, 'bar');
        $sut->setRequest($request);
        $this->assertEquals('bar', (string) $sut);
    }

    public function test_filter_with_default_default(): void
    {
        $request = new Request;
        $sut = new Filter('foo');
        $sut->setRequest($request);
        $this->assertEquals('', (string) $sut);
    }

    public function test_filter_with_int_conversion(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => '01']]);
        $sut = new Filter('foo', null, null, Conversion::INTEGER);
        $sut->setRequest($request);
        $this->assertEquals(1, $sut->get());
    }

    public function test_filter_with_string_conversion(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 1]]);
        $sut = new Filter('foo', null, null, Conversion::STRING);
        $sut->setRequest($request);
        $this->assertEquals('1', $sut->get());
    }

    public function test_set_value(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = new Filter('foo', null, 'biz');
        $sut->setRequest($request)
            ->setValue('baz');
        $this->assertEquals('baz', (string) $sut);
    }

    public function test_set_value_with_array(): void
    {
        $request = ['filter' => ['foo' => 'bar']];
        $sut = new Filter('foo', null, 'biz');
        $sut->setRequest($request)
            ->setValue('baz');
        $this->assertEquals('baz', (string) $sut);
    }
}
