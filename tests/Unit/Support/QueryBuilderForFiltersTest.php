<?php

namespace Tests\Smorken\QueryStringFilter\Unit\Support;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Model\Filters\FilterHandler;
use Smorken\QueryStringFilter\Collectors\Filters;
use Smorken\QueryStringFilter\Parts\Filter;
use Smorken\QueryStringFilter\Support\QueryBuilderForFilters;

class QueryBuilderForFiltersTest extends TestCase
{
    public function test_default_for_array(): void
    {
        $sut = new QueryBuilderForFilters;
        $query = m::mock(Builder::class);
        $request = (new Request)->merge(['filter' => ['foo' => [1, 2, 3]]]);
        $filters = new Filters($request, [
            new Filter('foo'),
            new Filter('notId', 'id'),
        ]);
        $query->expects()->whereIn('foo', [1, 2, 3])->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $filters));
    }

    public function test_default_for_non_array(): void
    {
        $sut = new QueryBuilderForFilters;
        $query = m::mock(Builder::class);
        $request = (new Request)->merge(['filter' => ['foo' => 'bar', 'notId' => 1]]);
        $filters = new Filters($request, [
            new Filter('foo'),
            new Filter('notId', 'id'),
        ]);
        $query->expects()->where('foo', '=', 'bar')->once()->andReturnSelf();
        $query->expects()->where('id', '=', 1)->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $filters));
    }

    public function test_default_for_non_array_from_request_array(): void
    {
        $sut = new QueryBuilderForFilters;
        $query = m::mock(Builder::class);
        $request = ['filter' => ['foo' => 'bar', 'notId' => 1]];
        $filters = new Filters($request, [
            new Filter('foo'),
            new Filter('notId', 'id'),
        ]);
        $query->expects()->where('foo', '=', 'bar')->once()->andReturnSelf();
        $query->expects()->where('id', '=', 1)->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $filters));
    }

    public function test_from_handlers(): void
    {
        $sut = new QueryBuilderForFilters([
            new FilterHandler('foo', fn (Builder $query, mixed $value) => $query->foo($value)),
            new FilterHandler('id', fn (Builder $query, mixed $value) => $query->id($value)),
        ]);
        $query = m::mock(Builder::class);
        $request = (new Request)->merge(['filter' => ['foo' => 'bar', 'notId' => 1]]);
        $filters = new Filters($request, [
            new Filter('foo'),
            new Filter('notId', 'id'),
        ]);
        $query->expects()->foo('bar')->once()->andReturnSelf();
        $query->expects()->id(1)->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $filters));
    }

    public function test_from_handlers_from_request_array(): void
    {
        $sut = new QueryBuilderForFilters([
            new FilterHandler('foo', fn (Builder $query, mixed $value) => $query->foo($value)),
            new FilterHandler('id', fn (Builder $query, mixed $value) => $query->id($value)),
        ]);
        $query = m::mock(Builder::class);
        $request = ['filter' => ['foo' => 'bar', 'notId' => 1]];
        $filters = new Filters($request, [
            new Filter('foo'),
            new Filter('notId', 'id'),
        ]);
        $query->expects()->foo('bar')->once()->andReturnSelf();
        $query->expects()->id(1)->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $filters));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
