<?php

namespace Tests\Smorken\QueryStringFilter\Unit\Support;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\QueryStringFilter\Collectors\Withs;
use Smorken\QueryStringFilter\Parts\With;
use Smorken\QueryStringFilter\Support\QueryBuilderForWiths;

class QueryBuilderForWithsTest extends TestCase
{
    public function test_defaults_for_many(): void
    {
        $sut = new QueryBuilderForWiths;
        $request = (new Request)->merge(['include' => ['foo', 'bar']]);
        $withs = new Withs($request, [
            new With('foo'),
            new With('bar'),
        ]);
        $query = m::mock(Builder::class);
        $query->expects()->with('foo')->once()->andReturnSelf();
        $query->expects()->with('bar')->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $withs));
    }

    public function test_defaults_for_many_from_request_array(): void
    {
        $sut = new QueryBuilderForWiths;
        $request = ['include' => ['foo', 'bar']];
        $withs = new Withs($request, [
            new With('foo'),
            new With('bar'),
        ]);
        $query = m::mock(Builder::class);
        $query->expects()->with('foo')->once()->andReturnSelf();
        $query->expects()->with('bar')->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $withs));
    }

    public function test_defaults_for_many_ignores_not_included(): void
    {
        $sut = new QueryBuilderForWiths;
        $request = (new Request)->merge(['include' => ['foo', 'bar']]);
        $withs = new Withs($request, [
            new With('foo'),
        ]);
        $query = m::mock(Builder::class);
        $query->expects()->with('foo')->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $withs));
    }

    public function test_defaults_for_one(): void
    {
        $sut = new QueryBuilderForWiths;
        $request = (new Request)->merge(['include' => 'foo']);
        $withs = new Withs($request, [
            new With('foo'),
        ]);
        $query = m::mock(Builder::class);
        $query->expects()->with('foo')->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $withs));
    }

    public function test_defaults_for_one_with_model_key(): void
    {
        $sut = new QueryBuilderForWiths;
        $request = (new Request)->merge(['include' => 'foo']);
        $withs = new Withs($request, [
            new With('foo', 'otherKey'),
        ]);
        $query = m::mock(Builder::class);
        $query->expects()->with('otherKey')->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $withs));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
