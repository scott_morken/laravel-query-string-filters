<?php

namespace Tests\Smorken\QueryStringFilter\Unit\Support;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\QueryStringFilter\Collectors\Sorts;
use Smorken\QueryStringFilter\Parts\Sort;
use Smorken\QueryStringFilter\Support\QueryBuilderForSorts;

class QueryBuilderForSortsTest extends TestCase
{
    public function test_default_applied_for_one(): void
    {
        $sut = new QueryBuilderForSorts;
        $request = (new Request)->merge(['sort' => '-foo']);
        $sorts = new Sorts($request, [
            new Sort('foo'),
        ]);
        $query = m::mock(Builder::class);
        $query->expects()->orderBy('foo', 'desc')->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $sorts));
    }

    public function test_default_applied_for_one_from_request_array(): void
    {
        $sut = new QueryBuilderForSorts;
        $request = ['sort' => '-foo'];
        $sorts = new Sorts($request, [
            new Sort('foo'),
        ]);
        $query = m::mock(Builder::class);
        $query->expects()->orderBy('foo', 'desc')->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $sorts));
    }

    public function test_default_applied_for_one_with_model_key(): void
    {
        $sut = new QueryBuilderForSorts;
        $request = (new Request)->merge(['sort' => '-foo']);
        $sorts = new Sorts($request, [
            new Sort('foo', 'otherFoo'),
        ]);
        $query = m::mock(Builder::class);
        $query->expects()->orderBy('otherFoo', 'desc')->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $sorts));
    }

    public function test_default_applied_for_many(): void
    {
        $sut = new QueryBuilderForSorts;
        $request = (new Request)->merge(['sort' => ['-foo', 'bar']]);
        $sorts = new Sorts($request, [
            new Sort('foo'),
            new Sort('bar'),
        ]);
        $query = m::mock(Builder::class);
        $query->expects()->orderBy('foo', 'desc')->once()->andReturnSelf();
        $query->expects()->orderBy('bar', 'asc')->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $sorts));
    }

    public function test_default_ignores_not_included(): void
    {
        $sut = new QueryBuilderForSorts;
        $request = (new Request)->merge(['sort' => ['-foo', 'bar']]);
        $sorts = new Sorts($request, [
            new Sort('foo'),
        ]);
        $query = m::mock(Builder::class);
        $query->expects()->orderBy('foo', 'desc')->once()->andReturnSelf();
        $this->assertEquals($query, $sut->apply($query, $sorts));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
