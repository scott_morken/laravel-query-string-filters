<?php

namespace Tests\Smorken\QueryStringFilter\Unit\Collectors;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Smorken\QueryStringFilter\Collectors\Filters;
use Smorken\QueryStringFilter\Parts\Filter;

class FiltersTest extends TestCase
{
    public function test_add_filter(): void
    {
        $request = new Request;
        $filter = new Filter('foo');
        $sut = new Filters($request, []);
        $sut->add($filter);
        $this->assertSame($filter, $sut->find('foo'));
    }

    public function test_get_is_filter(): void
    {
        $request = new Request;
        $filter = new Filter('foo');
        $sut = new Filters($request, [
            $filter,
        ]);
        $this->assertSame($filter, $sut->find('foo'));
    }

    public function test_get_is_null_when_not_found(): void
    {
        $request = new Request;
        $sut = new Filters($request, [
            new Filter('foo'),
        ]);
        $this->assertNull($sut->find('bar'));
    }

    public function test_to_array(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar', 'fizz' => 'buzz']]);
        $fooFilter = new Filter('foo');
        $fizzFilter = new Filter('fizz');
        $buzzFilter = new Filter('buzz');
        $sut = new Filters($request, [
            $fooFilter,
            'otherFizz' => $fizzFilter,
            $buzzFilter,
        ]);
        $this->assertEquals([
            'foo' => 'bar',
            'fizz' => 'buzz',
        ], $sut->toArray());
    }

    public function test_to_array_from_array(): void
    {
        $request = ['filter' => ['foo' => 'bar', 'fizz' => 'buzz']];
        $fooFilter = new Filter('foo');
        $fizzFilter = new Filter('fizz');
        $buzzFilter = new Filter('buzz');
        $sut = new Filters($request, [
            $fooFilter,
            'otherFizz' => $fizzFilter,
            $buzzFilter,
        ]);
        $this->assertEquals([
            'foo' => 'bar',
            'fizz' => 'buzz',
        ], $sut->toArray());
    }

    public function test_to_array_with_skip_key(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar', 'fizz' => 'buzz']]);
        $fooFilter = new Filter('foo');
        $fizzFilter = new Filter('fizz');
        $buzzFilter = new Filter('buzz');
        $f = new Filters($request, [
            $fooFilter,
            'otherFizz' => $fizzFilter,
            $buzzFilter,
        ]);
        $sut = $f->filtered(['fizz']);
        $this->assertEquals([
            'foo' => 'bar',
        ], $sut->toArray());
    }

    public function test_zero_is_a_valid_filter(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => '0']]);
        $filter = new Filter('foo');
        $sut = new Filters($request, []);
        $sut->add($filter);
        $this->assertSame(['foo' => '0'], $sut->toArray());
    }
}
