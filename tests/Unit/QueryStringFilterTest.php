<?php

namespace Tests\Smorken\QueryStringFilter\Unit;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\QueryStringFilter\Constants\PartType;
use Smorken\QueryStringFilter\Parts\Filter;
use Smorken\QueryStringFilter\QueryStringFilter;
use Smorken\QueryStringFilter\Support\ValidationHelper;

class QueryStringFilterTest extends TestCase
{
    #[Test]
    public function it_can_copy_a_filter(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = QueryStringFilter::from($request)
            ->setFilters([new Filter('foo')]);
        $copy = $sut->copy();
        $this->assertNotSame($copy, $sut);
        $this->assertNotSame($copy->filter('foo'), $sut->filter('foo'));
    }

    #[Test]
    public function it_can_fail_validation_on_a_filter(): void
    {
        $validationFactory = new Factory(new Translator(new ArrayLoader, 'en'));
        ValidationHelper::setValidator($validationFactory);
        Validator::shouldReceive()->make()->andReturn($validationFactory);
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('validation.integer');
        $sut = QueryStringFilter::from($request)
            ->setFilters(['foo'])
            ->validate(['foo' => 'integer']);
    }

    #[Test]
    public function it_can_fail_validation_on_a_filter_from_a_request_array(): void
    {
        $validationFactory = new Factory(new Translator(new ArrayLoader, 'en'));
        ValidationHelper::setValidator($validationFactory);
        Validator::shouldReceive()->make()->andReturn($validationFactory);
        $request = ['filter' => ['foo' => 'bar']];
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('validation.integer');
        $sut = QueryStringFilter::from($request)
            ->setFilters(['foo'])
            ->validate(['foo' => 'integer']);
    }

    #[Test]
    public function it_can_validate_a_filter(): void
    {
        $validationFactory = new Factory(new Translator(new ArrayLoader, 'en'));
        ValidationHelper::setValidator($validationFactory);
        Validator::shouldReceive()->make()->andReturn($validationFactory);
        $request = (new Request)->merge(['filter' => ['foo' => '99']]);
        $sut = QueryStringFilter::from($request)
            ->setFilters(['foo'])
            ->validate(['foo' => 'integer']);
        $this->assertEquals('99', $sut->filter('foo'));
    }

    #[Test]
    public function it_can_validate_a_filter_from_a_request_array(): void
    {
        $validationFactory = new Factory(new Translator(new ArrayLoader, 'en'));
        ValidationHelper::setValidator($validationFactory);
        Validator::shouldReceive()->make()->andReturn($validationFactory);
        $request = ['filter' => ['foo' => '99']];
        $sut = QueryStringFilter::from($request)
            ->setFilters(['foo'])
            ->validate(['foo' => 'integer']);
        $this->assertEquals('99', $sut->filter('foo'));
    }

    public function test_empty_to_array(): void
    {
        $request = new Request;
        $sut = new QueryStringFilter($request);
        $this->assertEquals([], $sut->toArray());
    }

    public function test_empty_to_array_from_request_array(): void
    {
        $request = [];
        $sut = new QueryStringFilter($request);
        $this->assertEquals([], $sut->toArray());
    }

    public function test_filters_from_array_of_filters_without_hidden(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = QueryStringFilter::from($request)
            ->setFilters([new Filter('foo')]);
        $this->assertEquals([
            'filter' => ['foo' => 'bar'],
        ], $sut->toArray());
    }

    public function test_filters_from_array_of_filters_without_hidden_from_request_array(): void
    {
        $request = ['filter' => ['foo' => 'bar']];
        $sut = QueryStringFilter::from($request)
            ->setFilters([new Filter('foo')]);
        $this->assertEquals([
            'filter' => ['foo' => 'bar'],
        ], $sut->toArray());
    }

    public function test_filters_from_array_with_hidden(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = QueryStringFilter::from($request)
            ->setFilters(['foo'])
            ->setHidden(['foo']);
        $this->assertEquals([], $sut->toArray());
    }

    public function test_filters_from_array_with_hidden_from_request_array(): void
    {
        $request = ['filter' => ['foo' => 'bar']];
        $sut = QueryStringFilter::from($request)
            ->setFilters(['foo'])
            ->setHidden(['foo']);
        $this->assertEquals([], $sut->toArray());
    }

    public function test_filters_from_array_with_internal_key(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = QueryStringFilter::from($request)
            ->setFilters(['fooey' => 'foo']);
        $this->assertEquals([
            'filter' => ['foo' => 'bar'],
        ], $sut->toArray());
    }

    public function test_filters_from_array_with_internal_key_from_request_array(): void
    {
        $request = ['filter' => ['foo' => 'bar']];
        $sut = QueryStringFilter::from($request)
            ->setFilters(['fooey' => 'foo']);
        $this->assertEquals([
            'filter' => ['foo' => 'bar'],
        ], $sut->toArray());
    }

    public function test_filters_from_array_without_hidden(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = QueryStringFilter::from($request)
            ->setFilters(['foo']);
        $this->assertEquals([
            'filter' => ['foo' => 'bar'],
        ], $sut->toArray());
    }

    public function test_filters_from_array_without_hidden_from_request_array(): void
    {
        $request = ['filter' => ['foo' => 'bar']];
        $sut = QueryStringFilter::from($request)
            ->setFilters(['foo']);
        $this->assertEquals([
            'filter' => ['foo' => 'bar'],
        ], $sut->toArray());
    }

    public function test_find_filter(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = QueryStringFilter::from($request)
            ->setFilters([new Filter('foo')]);
        $this->assertEquals('bar', $sut->find('foo', PartType::FILTER));
    }

    public function test_find_filter_from_request_array(): void
    {
        $request = ['filter' => ['foo' => 'bar']];
        $sut = QueryStringFilter::from($request)
            ->setFilters([new Filter('foo')]);
        $this->assertEquals('bar', $sut->find('foo', PartType::FILTER));
    }

    public function test_find_sort_against_filter_is_null(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = QueryStringFilter::from($request)
            ->setFilters([new Filter('foo')]);
        $this->assertEquals(null, $sut->find('foo', PartType::SORT));
    }

    public function test_find_sort_against_filter_is_null_from_request_array(): void
    {
        $request = ['filter' => ['foo' => 'bar']];
        $sut = QueryStringFilter::from($request)
            ->setFilters([new Filter('foo')]);
        $this->assertEquals(null, $sut->find('foo', PartType::SORT));
    }

    public function test_get_filter_from_filters_by_key(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = QueryStringFilter::from($request)
            ->setFilters(['fooey' => 'foo']);
        $this->assertInstanceOf(\Smorken\QueryStringFilter\Contracts\Parts\Filter::class,
            $sut->filter('fooey'));
    }

    public function test_get_filter_from_filters_by_key_from_request_array(): void
    {
        $request = ['filter' => ['foo' => 'bar']];
        $sut = QueryStringFilter::from($request)
            ->setFilters(['fooey' => 'foo']);
        $this->assertInstanceOf(\Smorken\QueryStringFilter\Contracts\Parts\Filter::class,
            $sut->filter('fooey'));
    }

    public function test_get_filter_from_filters_by_part_key(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = QueryStringFilter::from($request)
            ->setFilters(['fooey' => 'foo']);
        $this->assertInstanceOf(\Smorken\QueryStringFilter\Contracts\Parts\Filter::class,
            $sut->filter('foo'));
    }

    public function test_get_filter_from_filters_by_part_key_from_request_array(): void
    {
        $request = ['filter' => ['foo' => 'bar']];
        $sut = QueryStringFilter::from($request)
            ->setFilters(['fooey' => 'foo']);
        $this->assertInstanceOf(\Smorken\QueryStringFilter\Contracts\Parts\Filter::class,
            $sut->filter('foo'));
    }

    public function test_key_values_from_array_without_hidden(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar'], 'page' => 1]);
        $sut = QueryStringFilter::from($request)
            ->setFilters(['foo'])
            ->setKeyValues(['page']);
        $this->assertEquals([
            'filter' => ['foo' => 'bar'],
            'page' => 1,
        ], $sut->toArray());
    }

    public function test_key_values_from_array_without_hidden_from_request_array(): void
    {
        $request = ['filter' => ['foo' => 'bar'], 'page' => 1];
        $sut = QueryStringFilter::from($request)
            ->setFilters(['foo'])
            ->setKeyValues(['page']);
        $this->assertEquals([
            'filter' => ['foo' => 'bar'],
            'page' => 1,
        ], $sut->toArray());
    }

    public function test_qualified_key_name_for_filter(): void
    {
        $request = (new Request)->merge(['filter' => ['foo' => 'bar']]);
        $sut = QueryStringFilter::from($request);
        $this->assertEquals('filter.foo', $sut->qualifiedKeyName('foo'));
    }

    public function test_qualified_key_name_for_filter_from_request_array(): void
    {
        $request = ['filter' => ['foo' => 'bar']];
        $sut = QueryStringFilter::from($request);
        $this->assertEquals('filter.foo', $sut->qualifiedKeyName('foo'));
    }

    public function test_sorts_from_many_array_with_hidden(): void
    {
        $request = (new Request)->merge(['sort' => ['-foo', '+bar']]);
        $sut = QueryStringFilter::from($request)
            ->setSorts(['foo', 'bar'])
            ->setHidden(['foo']);
        $this->assertEquals([
            'sort' => '+bar',
        ], $sut->toArray());
    }

    public function test_sorts_from_many_array_with_hidden_from_request_array(): void
    {
        $request = ['sort' => ['-foo', '+bar']];
        $sut = QueryStringFilter::from($request)
            ->setSorts(['foo', 'bar'])
            ->setHidden(['foo']);
        $this->assertEquals([
            'sort' => '+bar',
        ], $sut->toArray());
    }

    public function test_sorts_from_many_array_without_hidden(): void
    {
        $request = (new Request)->merge(['sort' => ['-foo', '+bar']]);
        $sut = QueryStringFilter::from($request)
            ->setSorts(['foo', 'bar']);
        $this->assertEquals([
            'sort' => ['-foo', '+bar'],
        ], $sut->toArray());
    }

    public function test_sorts_from_many_array_without_hidden_from_request_array(): void
    {
        $request = ['sort' => ['-foo', '+bar']];
        $sut = QueryStringFilter::from($request)
            ->setSorts(['foo', 'bar']);
        $this->assertEquals([
            'sort' => ['-foo', '+bar'],
        ], $sut->toArray());
    }

    public function test_sorts_from_one_array_without_hidden(): void
    {
        $request = (new Request)->merge(['sort' => '-foo']);
        $sut = QueryStringFilter::from($request)
            ->setSorts(['foo']);
        $this->assertEquals([
            'sort' => '-foo',
        ], $sut->toArray());
    }

    public function test_sorts_from_one_array_without_hidden_from_request_array(): void
    {
        $request = ['sort' => '-foo'];
        $sut = QueryStringFilter::from($request)
            ->setSorts(['foo']);
        $this->assertEquals([
            'sort' => '-foo',
        ], $sut->toArray());
    }

    public function test_withs_from_many_array_with_hidden(): void
    {
        $request = (new Request)->merge(['include' => ['foo', 'bar']]);
        $sut = QueryStringFilter::from($request)
            ->setWiths(['foo', 'bar'])
            ->setHidden(['foo']);
        $this->assertEquals([
            'include' => 'bar',
        ], $sut->toArray());
    }

    public function test_withs_from_many_array_with_hidden_from_request_array(): void
    {
        $request = ['include' => ['foo', 'bar']];
        $sut = QueryStringFilter::from($request)
            ->setWiths(['foo', 'bar'])
            ->setHidden(['foo']);
        $this->assertEquals([
            'include' => 'bar',
        ], $sut->toArray());
    }

    public function test_withs_from_many_array_without_hidden(): void
    {
        $request = (new Request)->merge(['include' => ['foo', 'bar']]);
        $sut = QueryStringFilter::from($request)
            ->setWiths(['foo', 'bar']);
        $this->assertEquals([
            'include' => ['foo', 'bar'],
        ], $sut->toArray());
    }

    public function test_withs_from_many_array_without_hidden_from_request_array(): void
    {
        $request = ['include' => ['foo', 'bar']];
        $sut = QueryStringFilter::from($request)
            ->setWiths(['foo', 'bar']);
        $this->assertEquals([
            'include' => ['foo', 'bar'],
        ], $sut->toArray());
    }

    public function test_withs_from_one_array_without_hidden(): void
    {
        $request = (new Request)->merge(['include' => 'foo']);
        $sut = QueryStringFilter::from($request)
            ->setWiths(['foo']);
        $this->assertEquals([
            'include' => 'foo',
        ], $sut->toArray());
    }

    public function test_withs_from_one_array_without_hidden_from_request_array(): void
    {
        $request = ['include' => 'foo'];
        $sut = QueryStringFilter::from($request)
            ->setWiths(['foo']);
        $this->assertEquals([
            'include' => 'foo',
        ], $sut->toArray());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
