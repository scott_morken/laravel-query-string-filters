<?php

namespace Tests\Smorken\Domain\Concerns;

use Illuminate\Database\ConnectionInterface;
use Mockery as m;
use Tests\Smorken\Domain\Stubs\MockPDO;

trait WithMockConnection
{
    protected \PDOStatement|m\MockInterface|null $statement = null;

    protected function getMockConnection(
        string $connectionClass,
        array $config = [],
        array $methods = []
    ): ConnectionInterface {
        $this->statement = null;
        $this->statement = m::mock(\PDOStatement::class);
        $this->statement->shouldReceive('setFetchMode');
        $pdo = m::mock(MockPDO::class);
        $connection = $this->getMockBuilder($connectionClass)
            ->onlyMethods($methods)
            ->setConstructorArgs([$pdo, '', '', $config])
            ->getMock();
        $connection->enableQueryLog();

        return $connection;
    }
}
