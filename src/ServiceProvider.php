<?php

declare(strict_types=1);

namespace Smorken\QueryStringFilter;

use Illuminate\Contracts\Validation\Factory;
use Smorken\QueryStringFilter\Support\ValidationHelper;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        ValidationHelper::setValidator($this->app[Factory::class]);
    }
}
