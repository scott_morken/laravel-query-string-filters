<?php

namespace Smorken\QueryStringFilter;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\QueryStringFilter\Constants\PartType;
use Smorken\QueryStringFilter\Contracts\Collectors\Filters;
use Smorken\QueryStringFilter\Contracts\Collectors\KeyValues;
use Smorken\QueryStringFilter\Contracts\Collectors\Sorts;
use Smorken\QueryStringFilter\Contracts\Collectors\Withs;
use Smorken\QueryStringFilter\Contracts\Parts\Filter;
use Smorken\QueryStringFilter\Contracts\Parts\KeyValue;
use Smorken\QueryStringFilter\Contracts\Parts\Part;
use Smorken\QueryStringFilter\Contracts\Parts\Sort;
use Smorken\QueryStringFilter\Contracts\Parts\With;
use Smorken\QueryStringFilter\Support\CollectorsToArray;
use Smorken\QueryStringFilter\Support\CollectorsToCollection;
use Smorken\QueryStringFilter\Support\PartFindHelper;
use Smorken\QueryStringFilter\Support\PartNameHelper;
use Smorken\QueryStringFilter\Support\ValidationHelper;

final class QueryStringFilter implements \Smorken\QueryStringFilter\Contracts\QueryStringFilter
{
    protected Filters $filters;

    protected array $hidden = [];

    protected KeyValues $keyValues;

    protected PartFindHelper $partFindHelper;

    protected PartNameHelper $partNameHelper;

    protected Sorts $sorts;

    protected ValidationHelper $validationHelper;

    protected Withs $withs;

    public function __construct(protected Request|array $request)
    {
        $this->filters = new Collectors\Filters($this->request, []);
        $this->keyValues = new \Smorken\QueryStringFilter\Collectors\KeyValues($this->request, []);
        $this->sorts = new Collectors\Sorts($this->request, []);
        $this->withs = new Collectors\Withs($this->request, []);
        $this->partFindHelper = new PartFindHelper($this);
        $this->partNameHelper = new PartNameHelper;
        $this->validationHelper = new ValidationHelper;
    }

    public static function from(Request|array $request): self
    {
        return new self($request);
    }

    public function addFilter(Filter|string $filter, ?string $modelKey = null): Contracts\QueryStringFilter
    {
        $this->filters->add($filter, $modelKey);

        return $this;
    }

    public function addKeyValue(KeyValue|string $keyValue, ?string $modelKey = null): Contracts\QueryStringFilter
    {
        $this->keyValues->add($keyValue, $modelKey);

        return $this;
    }

    public function addSort(Sort|string $sort, ?string $modelKey = null): Contracts\QueryStringFilter
    {
        $this->sorts->add($sort, $modelKey);

        return $this;
    }

    public function addWith(With|string $with, ?string $modelKey = null): Contracts\QueryStringFilter
    {
        $this->withs->add($with, $modelKey);

        return $this;
    }

    public function copy(): self
    {
        $i = new self($this->request);
        foreach ($this->filters(true)->all() as $filter) {
            $i->addFilter(clone $filter);
        }
        foreach ($this->keyValues(true)->all() as $keyValue) {
            $i->addKeyValue(clone $keyValue);
        }
        foreach ($this->sorts(true)->all() as $sort) {
            $i->addSort(clone $sort);
        }
        foreach ($this->withs(true)->all() as $with) {
            $i->addWith(clone $with);
        }
        $i->hidden = $this->hidden;

        return $i;
    }

    public function filter(string $key): ?Filter
    {
        /** @var ?Filter */
        return $this->filters->find($key);
    }

    public function filters(bool $withHidden = false): Filters
    {
        return $this->filters->filtered($this->getSkipKeys($withHidden));
    }

    public function find(string $key, PartType $type): ?Part
    {
        return $this->partFindHelper->find($key, $type);
    }

    public function has(string $key, PartType $type): bool
    {
        return $this->partFindHelper->has($key, $type);
    }

    public function isHidden(string $key): bool
    {
        return in_array($key, $this->hidden, true);
    }

    public function keyValue(string $key): ?KeyValue
    {
        /** @var ?KeyValue */
        return $this->keyValues->find($key);
    }

    public function keyValues(bool $withHidden = false): KeyValues
    {
        return $this->keyValues->filtered($this->getSkipKeys($withHidden));
    }

    public function qualifiedKeyName(string $key, PartType $type = PartType::FILTER): string
    {
        return $this->partNameHelper->get($key, $type);
    }

    public function request(): Request|array
    {
        return $this->request;
    }

    public function setFilters(iterable $filters): self
    {
        $this->filters->set($filters);

        return $this;
    }

    public function setHidden(array $keys): self
    {
        $this->hidden = array_unique([...$this->hidden, ...$keys]);

        return $this;
    }

    public function setKeyValues(iterable $keyValues): Contracts\QueryStringFilter
    {
        $this->keyValues->set($keyValues);

        return $this;
    }

    public function setSorts(iterable $sorts): self
    {
        $this->sorts->set($sorts);

        return $this;
    }

    public function setWiths(iterable $withs): self
    {
        $this->withs->set($withs);

        return $this;
    }

    public function sort(string $key): ?Sort
    {
        /** @var ?Sort */
        return $this->sorts->find($key);
    }

    public function sorts(bool $withHidden = false): Sorts
    {
        return $this->sorts->filtered($this->getSkipKeys($withHidden));
    }

    public function toArray(bool $withHidden = false): array
    {
        $helper = new CollectorsToArray;

        return $helper->addFilters($this->filters($withHidden))
            ->addSorts($this->sorts($withHidden))
            ->addWiths($this->withs($withHidden))
            ->addKeyValues($this->keyValues($withHidden))
            ->get();
    }

    public function toCollection(bool $withHidden = false, bool $filter = true): Collection
    {
        $helper = new CollectorsToCollection;

        return $helper->addFilters($this->filters($withHidden))
            ->addSorts($this->sorts($withHidden))
            ->addWiths($this->withs($withHidden))
            ->get($filter);
    }

    public function toQueryString(): string
    {
        return http_build_query($this->toArray());
    }

    public function validate(array $rules, PartType $type = PartType::FILTER): Contracts\QueryStringFilter
    {
        match ($type) {
            PartType::FILTER => $this->doValidate($this->filters()->toArray(), $rules),
            PartType::WITH => $this->doValidate($this->withs()->toArray(), $rules),
            PartType::SORT => $this->doValidate($this->sorts()->toArray(), $rules),
            PartType::KEY_VALUE => $this->doValidate($this->keyValues()->toArray(), $rules)
        };

        return $this;
    }

    public function with(string $key): ?With
    {
        /** @var ?With */
        return $this->withs->find($key);
    }

    public function withs(bool $withHidden = false): Withs
    {
        return $this->withs->filtered($this->getSkipKeys($withHidden));
    }

    protected function doValidate(array $data, array $rules): array
    {
        return $this->validationHelper->validate($data, $rules);
    }

    protected function getSkipKeys(bool $withHidden): array
    {
        return $withHidden === false ? $this->hidden : [];
    }

    protected function setup(): void
    {
        // override
    }
}
