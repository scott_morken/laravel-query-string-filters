<?php

namespace Smorken\QueryStringFilter\Support;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Contracts\QueryBuilders\InvokableQueryScope;
use Smorken\Model\Filters\FilterHandler;
use Smorken\QueryStringFilter\Contracts\Collectors\Sorts;
use Smorken\QueryStringFilter\Contracts\Parts\Sort;
use Smorken\QueryStringFilter\ValueObjects\SortVO;

class QueryBuilderForSorts
{
    public function __construct(protected array $handlers = [], protected $allowDefaults = true) {}

    public function apply(Builder $builder, Sorts $sorts): Builder
    {
        /** @var \Smorken\QueryStringFilter\Contracts\Parts\Sort $sort */
        foreach ($sorts->all() as $sort) {
            $handler = $this->getHandlerForSort($sort);
            if (! $handler) {
                continue;
            }
            $invokable = $this->createInvokableQueryBuilder($handler, $sort);
            $builder = $invokable($builder);
        }

        return $builder;
    }

    protected function createInvokableQueryBuilder(FilterHandler $handler, Sort $sort): InvokableQueryScope
    {
        $handlerClass = $handler->builderClass;
        $value = $sort->get();

        return new $handlerClass($handler->queryMethod, $value);
    }

    protected function getDefaultHandlerForSort(Sort $sort): FilterHandler
    {
        return new FilterHandler($sort->modelKey(),
            fn (Builder $query, SortVO $value) => $query->orderBy($sort->modelKey(), $value->direction->value));
    }

    protected function getHandlerForSort(Sort $sort): ?FilterHandler
    {
        /** @var FilterHandler $handler */
        foreach ($this->handlers as $handler) {
            if ($sort->matches($handler->filterKey)) {
                return $handler;
            }
        }

        return $this->allowDefaults ? $this->getDefaultHandlerForSort($sort) : null;
    }
}
