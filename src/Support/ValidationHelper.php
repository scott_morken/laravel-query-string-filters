<?php

declare(strict_types=1);

namespace Smorken\QueryStringFilter\Support;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Validation\ValidationException;

class ValidationHelper
{
    protected static ?Factory $factory = null;

    public static function setValidator(Factory $factory): void
    {
        self::$factory = $factory;
    }

    public function validate(array $data, array $rules): array
    {
        if (($validator = $this->getValidator()) === null) {
            throw ValidationException::withMessages(['validator' => 'The validation factory has not been initialized.']);
        }
        $v = $validator->make($data, $rules);

        return $v->validate();
    }

    protected function getValidator(): ?Factory
    {
        return self::$factory;
    }
}
