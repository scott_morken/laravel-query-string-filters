<?php

declare(strict_types=1);

namespace Smorken\QueryStringFilter\Support;

use Smorken\QueryStringFilter\Concerns\Conversion;

class ConversionHelper
{
    public function convert(mixed $value, Conversion $conversion): mixed
    {
        return match ($conversion) {
            Conversion::ARRAY => (array) $value,
            Conversion::BOOLEAN => (bool) $value,
            Conversion::INTEGER => (int) $value,
            Conversion::FLOAT => (float) $value,
            Conversion::STRING => (string) $value
        };
    }
}
