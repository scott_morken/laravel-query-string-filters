<?php

namespace Smorken\QueryStringFilter\Support;

use Illuminate\Support\Collection;
use Smorken\QueryStringFilter\Contracts\Collectors\Collector;
use Smorken\QueryStringFilter\Contracts\Collectors\Filters;
use Smorken\QueryStringFilter\Contracts\Collectors\Sorts;
use Smorken\QueryStringFilter\Contracts\Collectors\Withs;
use Smorken\QueryStringFilter\Parts\Filter;
use Smorken\QueryStringFilter\Parts\Sort;
use Smorken\QueryStringFilter\Parts\With;

class CollectorsToCollection
{
    protected Collection $collection;

    public function __construct()
    {
        $this->collection = new Collection;
    }

    public function addFilters(Filters $filters): self
    {
        $this->collection->put(Filter::getBaseRequestKey(), $filters);

        return $this;
    }

    public function addSorts(Sorts $sorts): self
    {
        $this->collection->put(Sort::getBaseRequestKey(), $sorts);

        return $this;
    }

    public function addWiths(Withs $withs): self
    {
        $this->collection->put(With::getBaseRequestKey(), $withs);

        return $this;
    }

    public function get(bool $filter = true): Collection
    {
        if (! $filter) {
            return $this->collection;
        }

        return $this->collection->filter(fn (Collector $collector) => $collector->isNotEmpty());
    }
}
