<?php

namespace Smorken\QueryStringFilter\Support;

use Illuminate\Support\Collection;
use Smorken\QueryStringFilter\Constants\PartType;
use Smorken\QueryStringFilter\Contracts\Parts\Part;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;

class PartFindHelper
{
    protected Collection $cache;

    public function __construct(protected QueryStringFilter $filter)
    {
        $this->cache = new Collection;
    }

    public function find(string $key, PartType $type = PartType::FILTER): ?Part
    {
        return $this->findFromPart($key, $type);
    }

    public function has(string $key, PartType $type = PartType::FILTER): bool
    {
        $value = $this->findFromPart($key, $type);

        return $value !== null;
    }

    protected function findFromPart(string $key, PartType $type): ?Part
    {
        if ($this->isCached($key, $type)) {
            return $this->fromCache($key, $type);
        }
        $part = match ($type) {
            PartType::FILTER => $this->filter->filter($key),
            PartType::SORT => $this->filter->sort($key),
            PartType::WITH => $this->filter->with($key),
            default => $this->filter->keyValue($key)
        };
        $this->toCache($key, $type, $part);

        return $part;
    }

    protected function fromCache(string $key, PartType $type): ?Part
    {
        return $this->cache->get($this->getQualifiedKey($key, $type));
    }

    protected function getQualifiedKey(string $key, PartType $type): string
    {
        return sprintf('%s.%s', $type->value, $key);
    }

    protected function isCached(string $key, PartType $type): bool
    {
        return $this->cache->has($this->getQualifiedKey($key, $type));
    }

    protected function toCache(string $key, PartType $type, ?Part $part): void
    {
        $this->cache->put($this->getQualifiedKey($key, $type), $part);
    }
}
