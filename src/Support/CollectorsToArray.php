<?php

namespace Smorken\QueryStringFilter\Support;

use Smorken\QueryStringFilter\Contracts\Collectors\Filters;
use Smorken\QueryStringFilter\Contracts\Collectors\KeyValues;
use Smorken\QueryStringFilter\Contracts\Collectors\Sorts;
use Smorken\QueryStringFilter\Contracts\Collectors\Withs;
use Smorken\QueryStringFilter\Parts\Filter;
use Smorken\QueryStringFilter\Parts\Sort;
use Smorken\QueryStringFilter\Parts\With;
use Smorken\QueryStringFilter\ValueObjects\SortVO;

class CollectorsToArray
{
    protected array $collectors = [];

    public function addFilters(Filters $filters): self
    {
        $this->collectors[Filter::getBaseRequestKey()] = $filters->toArray();

        return $this;
    }

    public function addKeyValues(KeyValues $keyValues): self
    {
        foreach ($keyValues->toArray() as $key => $value) {
            $this->collectors[$key] = $value;
        }

        return $this;
    }

    public function addSorts(Sorts $sorts): self
    {
        $arr = array_values(array_map(fn (SortVO $vo) => (string) $vo, $sorts->toArray()));
        $this->collectors[Sort::getBaseRequestKey()] = count($arr) === 1 ? $arr[0] : $arr;

        return $this;
    }

    public function addWiths(Withs $withs): self
    {
        $arr = array_values($withs->toArray());
        $this->collectors[With::getBaseRequestKey()] = count($arr) === 1 ? $arr[0] : $arr;

        return $this;
    }

    public function get(): array
    {
        return array_filter($this->collectors);
    }
}
