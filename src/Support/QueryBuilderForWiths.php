<?php

namespace Smorken\QueryStringFilter\Support;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Contracts\QueryBuilders\InvokableQueryScope;
use Smorken\Model\Filters\FilterHandler;
use Smorken\QueryStringFilter\Contracts\Collectors\Withs;
use Smorken\QueryStringFilter\Contracts\Parts\With;

class QueryBuilderForWiths
{
    public function __construct(protected array $handlers = [], protected bool $allowDefaults = true) {}

    public function apply(Builder $builder, Withs $withs): Builder
    {
        /** @var \Smorken\QueryStringFilter\Contracts\Parts\With $with */
        foreach ($withs->all() as $with) {
            $handler = $this->getHandlerForWith($with);
            if (! $handler) {
                continue;
            }
            $invokable = $this->createInvokableQueryBuilder($handler, $with);
            $builder = $invokable($builder);
        }

        return $builder;
    }

    protected function createInvokableQueryBuilder(FilterHandler $handler, With $with): InvokableQueryScope
    {
        $handlerClass = $handler->builderClass;
        $value = $with->modelKey();

        return new $handlerClass($handler->queryMethod, $value);
    }

    protected function getDefaultHandlerForWith(With $with): FilterHandler
    {
        return new FilterHandler($with->modelKey(), fn (Builder $query, mixed $value) => $query->with($value));
    }

    protected function getHandlerForWith(With $with): ?FilterHandler
    {
        /** @var FilterHandler $handler */
        foreach ($this->handlers as $handler) {
            if ($with->matches($handler->filterKey)) {
                return $handler;
            }
        }

        return $this->allowDefaults ? $this->getDefaultHandlerForWith($with) : null;
    }
}
