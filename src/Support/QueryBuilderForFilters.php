<?php

namespace Smorken\QueryStringFilter\Support;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Contracts\QueryBuilders\InvokableQueryScope;
use Smorken\Model\Filters\FilterHandler;
use Smorken\QueryStringFilter\Contracts\Collectors\Filters;
use Smorken\QueryStringFilter\Contracts\Parts\Filter;

class QueryBuilderForFilters
{
    public function __construct(
        protected array $handlers = [],
        protected bool $allowDefaults = true
    ) {}

    public function apply(Builder $builder, Filters $filters): Builder
    {
        /** @var \Smorken\QueryStringFilter\Contracts\Parts\Filter $filter */
        foreach ($filters->all() as $filter) {
            $handler = $this->getHandlerForFilter($filter);
            if (! $handler) {
                continue;
            }
            $invokable = $this->createInvokableQueryBuilder($handler, $filter);
            $builder = $invokable($builder);
        }

        return $builder;
    }

    protected function createInvokableQueryBuilder(FilterHandler $handler, Filter $filter): InvokableQueryScope
    {
        $handlerClass = $handler->builderClass;
        $value = $filter->get();

        return new $handlerClass($handler->queryMethod, $value);
    }

    protected function getDefaultHandlerForFilter(Filter $filter): FilterHandler
    {
        $v = $filter->get();
        if (is_array($v)) {
            return new FilterHandler($filter->modelKey(),
                fn (Builder $query, array $value) => $query->whereIn($filter->modelKey(), $value));
        }

        return new FilterHandler($filter->modelKey(),
            fn (Builder $query, mixed $value) => $query->where($filter->modelKey(), '=', $value));
    }

    protected function getHandlerForFilter(Filter $filter): ?FilterHandler
    {
        /** @var FilterHandler $handler */
        foreach ($this->handlers as $handler) {
            if ($filter->matches($handler->filterKey)) {
                return $handler;
            }
        }

        return $this->allowDefaults ? $this->getDefaultHandlerForFilter($filter) : null;
    }
}
