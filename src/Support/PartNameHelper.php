<?php

namespace Smorken\QueryStringFilter\Support;

use Smorken\QueryStringFilter\Constants\PartType;
use Smorken\QueryStringFilter\Parts\Filter;
use Smorken\QueryStringFilter\Parts\KeyValue;
use Smorken\QueryStringFilter\Parts\Sort;
use Smorken\QueryStringFilter\Parts\With;

class PartNameHelper
{
    protected array $cached = [];

    public function get(string $key, PartType $type = PartType::FILTER): string
    {
        if ($this->isCached($key, $type)) {
            return $this->fromCache($key, $type);
        }
        $name = implode('.', array_filter([$this->getBaseNameByType($type), $key]));
        $this->toCache($key, $type, $name);

        return $name;
    }

    protected function fromCache(string $key, PartType $type): string
    {
        return $this->cached[$this->getCacheKey($key, $type)];
    }

    protected function getBaseNameByType(PartType $type): ?string
    {
        return match ($type) {
            PartType::FILTER => Filter::getBaseRequestKey(),
            PartType::SORT => Sort::getBaseRequestKey(),
            PartType::WITH => With::getBaseRequestKey(),
            PartType::KEY_VALUE => KeyValue::getBaseRequestKey()
        };
    }

    protected function getCacheKey(string $key, PartType $type): string
    {
        return sprintf('%s.%s', $type->value, $key);
    }

    protected function isCached(string $key, PartType $type): bool
    {
        return array_key_exists($this->getCacheKey($key, $type), $this->cached);
    }

    protected function toCache(string $key, PartType $type, string $name): void
    {
        $this->cached[$this->getCacheKey($key, $type)] = $name;
    }
}
