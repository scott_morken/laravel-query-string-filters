<?php

namespace Smorken\QueryStringFilter\ValueObjects;

use Smorken\QueryStringFilter\Constants\SortDirection;

class SortVO implements \Stringable
{
    public function __construct(
        public string $value,
        public SortDirection $direction
    ) {}

    public function __toString(): string
    {
        return sprintf('%s%s', $this->direction->toSymbol(), $this->value);
    }
}
