<?php

declare(strict_types=1);

namespace Smorken\QueryStringFilter\Concerns;

enum Conversion
{
    case STRING;

    case INTEGER;

    case FLOAT;

    case BOOLEAN;

    case ARRAY;
}
