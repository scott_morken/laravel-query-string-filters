<?php

namespace Smorken\QueryStringFilter\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\QueryStringFilter\Contracts\Collectors\Filters;
use Smorken\QueryStringFilter\Contracts\Collectors\Sorts;
use Smorken\QueryStringFilter\Contracts\Collectors\Withs;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\QueryStringFilter\Support\QueryBuilderForFilters;
use Smorken\QueryStringFilter\Support\QueryBuilderForSorts;
use Smorken\QueryStringFilter\Support\QueryBuilderForWiths;

trait WithQueryStringFilter
{
    public function queryStringFilter(QueryStringFilter $filter): Builder
    {
        $this->handleFilters($filter->filters(true));
        $this->handleSorts($filter->sorts(true));
        $this->handleWiths($filter->withs(true));

        return $this->getBuilder();
    }

    protected function allowDefaultsOnFilters(): bool
    {
        return true;
    }

    protected function allowDefaultsOnSorts(): bool
    {
        return true;
    }

    protected function allowDefaultsOnWiths(): bool
    {
        return true;
    }

    protected function getBuilder(): Builder
    {
        return $this;
    }

    protected function getFilterHandlersForFilters(): array
    {
        return [];
    }

    protected function getFilterHandlersForSorts(): array
    {
        return [];
    }

    protected function getFilterHandlersForWiths(): array
    {
        return [];
    }

    protected function handleFilters(Filters $filters): void
    {
        $helper = new QueryBuilderForFilters($this->getFilterHandlersForFilters(), $this->allowDefaultsOnFilters());
        $helper->apply($this->getBuilder(), $filters);
    }

    protected function handleSorts(Sorts $sorts): void
    {
        $helper = new QueryBuilderForSorts($this->getFilterHandlersForSorts(), $this->allowDefaultsOnSorts());
        $helper->apply($this->getBuilder(), $sorts);
    }

    protected function handleWiths(Withs $withs): void
    {
        $helper = new QueryBuilderForWiths($this->getFilterHandlersForWiths(), $this->allowDefaultsOnWiths());
        $helper->apply($this->getBuilder(), $withs);
    }
}
