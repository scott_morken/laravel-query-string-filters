<?php

namespace Smorken\QueryStringFilter\Constants;

enum PartType: string
{
    case FILTER = 'filter';
    case KEY_VALUE = 'key-value';

    case SORT = 'sort';

    case WITH = 'with';
}
