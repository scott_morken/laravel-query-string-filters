<?php

namespace Smorken\QueryStringFilter\Constants;

enum SortDirection: string
{
    case ASCENDING = 'asc';
    case DESCENDING = 'desc';

    public function toSymbol(): string
    {
        return match ($this) {
            self::ASCENDING => '+',
            self::DESCENDING => '-'
        };
    }
}
