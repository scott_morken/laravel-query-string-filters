<?php

namespace Smorken\QueryStringFilter\Collectors;

use Smorken\QueryStringFilter\Contracts\Parts\Part;
use Smorken\QueryStringFilter\Parts\Sort;

class Sorts extends Collector implements \Smorken\QueryStringFilter\Contracts\Collectors\Sorts
{
    protected function createPart(string $requestKey, ?string $modelKey): Part
    {
        return new Sort($requestKey, $modelKey);
    }
}
