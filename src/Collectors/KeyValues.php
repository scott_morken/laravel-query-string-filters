<?php

namespace Smorken\QueryStringFilter\Collectors;

use Smorken\QueryStringFilter\Contracts\Parts\Part;
use Smorken\QueryStringFilter\Parts\KeyValue;

class KeyValues extends Collector implements \Smorken\QueryStringFilter\Contracts\Collectors\KeyValues
{
    protected function createPart(string $requestKey, ?string $modelKey): Part
    {
        return new KeyValue($requestKey, $modelKey);
    }
}
