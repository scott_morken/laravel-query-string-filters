<?php

namespace Smorken\QueryStringFilter\Collectors;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\QueryStringFilter\Contracts\Parts\Part;

abstract class Collector implements \Smorken\QueryStringFilter\Contracts\Collectors\Collector
{
    protected Collection $collection;

    public function __construct(protected Request|array $request, iterable $parts)
    {
        $this->set($parts);
    }

    abstract protected function createPart(string $requestKey, ?string $modelKey): Part;

    public function add(string|Part $part, ?string $modelKey = null): self
    {
        if (is_string($part)) {
            $part = $this->createPart($part, $modelKey);
        }
        $part->setRequest($this->request);
        $this->all()->push($part);

        return $this;
    }

    public function all(): Collection
    {
        return $this->collection;
    }

    public function filtered(array $skipKeys = []): static
    {
        return new static(
            $this->request,
            $this->all()
                ->filter(fn (Part $part) => ! $part->empty())
                ->reject(fn (Part $part) => in_array($part->requestKey(), $skipKeys, true))
        );
    }

    public function find(string $key): ?Part
    {
        return $this->findByPartKey($key);
    }

    public function isEmpty(): bool
    {
        return $this->all()->isEmpty();
    }

    public function isNotEmpty(): bool
    {
        return $this->all()->isNotEmpty();
    }

    public function set(iterable $parts): void
    {
        $this->collection = new Collection;
        foreach ($parts as $modelKey => $part) {
            $this->add($part, is_string($modelKey) ? $modelKey : null);
        }
    }

    public function toArray(): array
    {
        return $this->filtered()
            ->all()
            ->mapWithKeys(fn (Part $part) => [$part->requestKey() => $part->get()])
            ->toArray();
    }

    protected function findByPartKey(string $key): ?Part
    {
        /** @var \Smorken\QueryStringFilter\Contracts\Parts\Part $part */
        foreach ($this->all() as $part) {
            if ($part->matches($key)) {
                return $part;
            }
        }

        return null;
    }
}
