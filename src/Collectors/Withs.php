<?php

namespace Smorken\QueryStringFilter\Collectors;

use Smorken\QueryStringFilter\Contracts\Parts\Part;
use Smorken\QueryStringFilter\Parts\With;

class Withs extends Collector implements \Smorken\QueryStringFilter\Contracts\Collectors\Withs
{
    protected function createPart(string $requestKey, ?string $modelKey): Part
    {
        return new With($requestKey, $modelKey);
    }
}
