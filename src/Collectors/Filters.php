<?php

namespace Smorken\QueryStringFilter\Collectors;

use Smorken\QueryStringFilter\Contracts\Parts\Part;
use Smorken\QueryStringFilter\Parts\Filter;

class Filters extends Collector implements \Smorken\QueryStringFilter\Contracts\Collectors\Filters
{
    protected function createPart(string $requestKey, ?string $modelKey): Part
    {
        return new Filter($requestKey, $modelKey);
    }
}
