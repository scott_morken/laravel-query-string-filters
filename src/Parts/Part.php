<?php

namespace Smorken\QueryStringFilter\Parts;

use Illuminate\Http\Request;
use Smorken\QueryStringFilter\Concerns\Conversion;
use Smorken\QueryStringFilter\Constants\PartType;
use Smorken\QueryStringFilter\Support\ConversionHelper;
use Smorken\QueryStringFilter\Support\PartNameHelper;

abstract class Part implements \Smorken\QueryStringFilter\Contracts\Parts\Part
{
    protected static string $arrayDelimiter = ',';

    protected static string $base;

    protected PartNameHelper $partNameHelper;

    protected PartType $partType;

    protected Request|array $request;

    protected mixed $value = null;

    protected bool $valueSet = false;

    public function __construct(
        protected string $requestKey,
        protected ?string $modelKey = null,
        protected mixed $default = null,
        protected ?Conversion $conversion = null
    ) {
        $this->partNameHelper = new PartNameHelper;
    }

    public static function getBaseRequestKey(): string
    {
        return static::$base;
    }

    public static function setArrayDelimiter(string $delimiter): void
    {
        static::$arrayDelimiter = $delimiter;
    }

    public static function setBaseRequestKey(string $base): void
    {
        static::$base = $base;
    }

    public function __toString(): string
    {
        $value = $this->get();
        if (is_array($value)) {
            $value = implode(static::$arrayDelimiter, $value);
        }
        if (is_a($value, \BackedEnum::class)) {
            $value = $value->value;
        }

        return (string) $value;
    }

    public function empty(): bool
    {
        return $this->isEmpty($this->get());
    }

    public function get(): mixed
    {
        if ($this->valueSet) {
            return $this->value;
        }
        $this->setValue($this->valueFromRequest($this->getQualifiedRequestKeyName(), $this->getDefault()));

        return $this->value;
    }

    public function htmlName(): string
    {
        return sprintf('%s[%s]', self::getBaseRequestKey(), $this->requestKey());
    }

    public function isType(PartType $partType): bool
    {
        return $this->type() === $partType;
    }

    public function matches(string $key): bool
    {
        return $this->requestKey === $key || $this->modelKey === $key;
    }

    public function modelKey(): string
    {
        return $this->modelKey ?? $this->requestKey();
    }

    public function request(): Request|array
    {
        return $this->request;
    }

    public function requestKey(): string
    {
        return $this->requestKey;
    }

    public function setRequest(Request|array $request): self
    {
        $this->request = $request;

        return $this;
    }

    public function setValue(mixed $value): self
    {
        $this->value = $this->convertValue($value);
        $this->valueSet = true;

        return $this;
    }

    public function type(): PartType
    {
        return $this->partType;
    }

    protected function convertValue(mixed $value): mixed
    {
        if ($this->conversion) {
            return (new ConversionHelper)->convert($value, $this->conversion);
        }

        return $value;
    }

    protected function getBaseArrayFromRequest(): array
    {
        return (array) $this->valueFromRequest(static::$base, []);
    }

    protected function getDefault(): mixed
    {
        if (property_exists($this, 'default')) {
            return $this->default;
        }

        return null;
    }

    protected function getQualifiedRequestKeyName(): string
    {
        return $this->partNameHelper->get($this->requestKey(), $this->type());
    }

    protected function isEmpty(mixed $value): bool
    {
        return empty($value) && ! is_numeric($value) && ! is_bool($value);
    }

    protected function valueFromRequest(string $keyName, mixed $default = null): mixed
    {
        $request = $this->request();
        if (is_array($request)) {
            return data_get($request, $keyName, $default);
        }

        return $this->request()->input($keyName, $default);
    }
}
