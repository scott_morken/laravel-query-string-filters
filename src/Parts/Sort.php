<?php

namespace Smorken\QueryStringFilter\Parts;

use Smorken\QueryStringFilter\Constants\PartType;
use Smorken\QueryStringFilter\Constants\SortDirection;
use Smorken\QueryStringFilter\ValueObjects\SortVO;

class Sort extends Part implements \Smorken\QueryStringFilter\Contracts\Parts\Sort
{
    protected static string $base = 'sort';

    protected PartType $partType = PartType::SORT;

    public function get(): ?SortVO
    {
        $v = $this->getMatchingFromRequest() ?? $this->getDefault();
        if ($v) {
            return new SortVO($this->requestKey(), $this->getDirectionFromValue($v));
        }

        return null;
    }

    protected function getDirectionFromValue(string $value): SortDirection
    {
        if (str_starts_with($value, SortDirection::DESCENDING->toSymbol())) {
            return SortDirection::DESCENDING;
        }

        return SortDirection::ASCENDING;
    }

    protected function getMatchingFromRequest(): ?string
    {
        foreach ($this->getBaseArrayFromRequest() as $value) {
            $v = $this->getValueFromValue($value);
            if ($this->matches($v)) {
                return $value;
            }
        }

        return null;
    }

    protected function getValueFromValue(string $value): string
    {
        if (str_starts_with($value, SortDirection::DESCENDING->toSymbol()) ||
            str_starts_with($value, SortDirection::ASCENDING->toSymbol())) {
            return substr($value, 1);
        }

        return $value;
    }
}
