<?php

namespace Smorken\QueryStringFilter\Parts;

use Smorken\QueryStringFilter\Constants\PartType;

class KeyValue extends Part implements \Smorken\QueryStringFilter\Contracts\Parts\KeyValue
{
    protected PartType $partType = PartType::KEY_VALUE;

    public function htmlName(): string
    {
        return $this->requestKey();
    }

    protected function getQualifiedRequestKeyName(): string
    {
        return $this->requestKey();
    }
}
