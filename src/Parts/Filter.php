<?php

namespace Smorken\QueryStringFilter\Parts;

use Smorken\QueryStringFilter\Constants\PartType;

class Filter extends Part implements \Smorken\QueryStringFilter\Contracts\Parts\Filter
{
    protected static string $base = 'filter';

    protected PartType $partType = PartType::FILTER;
}
