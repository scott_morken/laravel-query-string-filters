<?php

namespace Smorken\QueryStringFilter\Parts;

use Smorken\QueryStringFilter\Constants\PartType;

class With extends Part implements \Smorken\QueryStringFilter\Contracts\Parts\With
{
    protected static string $base = 'include';

    protected PartType $partType = PartType::WITH;

    public function get(): ?string
    {
        foreach ($this->getBaseArrayFromRequest() as $key) {
            if ($this->matches($key)) {
                return $this->requestKey;
            }
        }

        return null;
    }
}
