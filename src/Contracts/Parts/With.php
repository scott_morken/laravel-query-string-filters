<?php

namespace Smorken\QueryStringFilter\Contracts\Parts;

interface With extends Part
{
    public function get(): ?string;
}
