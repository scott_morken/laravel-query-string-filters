<?php

namespace Smorken\QueryStringFilter\Contracts\Parts;

use Smorken\QueryStringFilter\ValueObjects\SortVO;

interface Sort extends Part
{
    public function get(): ?SortVO;
}
