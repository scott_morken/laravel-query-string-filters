<?php

namespace Smorken\QueryStringFilter\Contracts\Parts;

use Illuminate\Http\Request;
use Smorken\QueryStringFilter\Constants\PartType;

/**
 * @property string $requestKey
 * @property mixed $default
 */
interface Part extends \Stringable
{
    public function empty(): bool;

    public function get(): mixed;

    public function htmlName(): string;

    public function isType(PartType $partType): bool;

    public function matches(string $key): bool;

    public function modelKey(): string;

    public function request(): Request|array;

    public function requestKey(): string;

    public function setRequest(Request|array $request): self;

    public function setValue(mixed $value): self;

    public function type(): PartType;

    public static function getBaseRequestKey(): string;

    public static function setArrayDelimiter(string $delimiter): void;

    public static function setBaseRequestKey(string $base): void;
}
