<?php

namespace Smorken\QueryStringFilter\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\QueryStringFilter\Constants\PartType;
use Smorken\QueryStringFilter\Contracts\Collectors\Filters;
use Smorken\QueryStringFilter\Contracts\Collectors\KeyValues;
use Smorken\QueryStringFilter\Contracts\Collectors\Sorts;
use Smorken\QueryStringFilter\Contracts\Collectors\Withs;
use Smorken\QueryStringFilter\Contracts\Parts\Filter;
use Smorken\QueryStringFilter\Contracts\Parts\KeyValue;
use Smorken\QueryStringFilter\Contracts\Parts\Part;
use Smorken\QueryStringFilter\Contracts\Parts\Sort;
use Smorken\QueryStringFilter\Contracts\Parts\With;

interface QueryStringFilter extends Arrayable
{
    public function addFilter(string|Filter $filter, ?string $modelKey = null): self;

    public function addKeyValue(string|KeyValue $keyValue, ?string $modelKey = null): self;

    public function addSort(string|Sort $sort, ?string $modelKey = null): self;

    public function addWith(string|With $with, ?string $modelKey = null): self;

    public function copy(): self;

    public function filter(string $key): ?Filter;

    public function filters(bool $withHidden = false): Filters;

    public function find(string $key, PartType $type): ?Part;

    public function has(string $key, PartType $type): bool;

    public function isHidden(string $key): bool;

    public function keyValue(string $key): ?KeyValue;

    public function keyValues(bool $withHidden = false): KeyValues;

    public function qualifiedKeyName(string $key, PartType $type = PartType::FILTER): string;

    public function request(): Request|array;

    public function setFilters(iterable $filters): self;

    public function setHidden(array $keys): self;

    public function setKeyValues(iterable $keyValues): self;

    public function setSorts(iterable $sorts): self;

    public function setWiths(iterable $withs): self;

    public function sort(string $key): ?Sort;

    public function sorts(bool $withHidden = false): Sorts;

    public function toArray(bool $withHidden = false): array;

    public function toCollection(bool $withHidden = false, bool $filter = true): Collection;

    public function toQueryString(): string;

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate(array $rules, PartType $type = PartType::FILTER): self;

    public function with(string $key): ?With;

    public function withs(bool $withHidden = false): Withs;

    public static function from(Request|array $request): self;
}
