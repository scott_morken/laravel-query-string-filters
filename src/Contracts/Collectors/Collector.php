<?php

namespace Smorken\QueryStringFilter\Contracts\Collectors;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\QueryStringFilter\Contracts\Parts\Part;

interface Collector extends Arrayable
{
    public function __construct(Request|array $request, iterable $parts);

    public function add(string|Part $part, ?string $modelKey = null): self;

    public function all(): Collection;

    public function filtered(array $skipKeys = []): static;

    public function find(string $key): ?Part;

    public function isEmpty(): bool;

    public function isNotEmpty(): bool;

    public function set(iterable $parts): void;

    public function toArray(): array;
}
